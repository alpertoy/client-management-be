package io.gitlab.alpertoy.backend.rest;

import io.gitlab.alpertoy.backend.model.dto.ProvidedServiceDTO;
import io.gitlab.alpertoy.backend.model.entity.Client;
import io.gitlab.alpertoy.backend.model.entity.ProvidedService;
import io.gitlab.alpertoy.backend.model.repository.ClientRepository;
import io.gitlab.alpertoy.backend.model.repository.ProvidedServiceRepository;
import io.gitlab.alpertoy.backend.util.BigDecimalConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * User: alpertoy
 * Date: 2.03.2021
 * Time: 14:14
 */

@RestController
@RequestMapping("/api/provided-services")
@RequiredArgsConstructor
public class ProvidedServiceController {

    private final ClientRepository clientRepository;
    private final ProvidedServiceRepository repository;
    private final BigDecimalConverter bigDecimalConverter;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ProvidedService save(@RequestBody @Valid ProvidedServiceDTO dto) {
        LocalDate date = LocalDate.parse(dto.getDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        Integer idClient = dto.getIdClient();

        Client client = clientRepository.findById(idClient)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Client does not exist"));

        ProvidedService providedService = new ProvidedService();
        providedService.setDescription(dto.getDescription());
        providedService.setDate(date);
        providedService.setClient(client);
        providedService.setValue(bigDecimalConverter.converter(dto.getPrice()));

        return repository.save(providedService);
    }

    @GetMapping
    public List<ProvidedService> search(
            @RequestParam(value = "name", required = false, defaultValue = "") String name,
            @RequestParam(value = "month", required = false) Integer month) {

        // where client.name like '%abc%'
        return repository.findByClientNameAndMonth("%" + name + "%", month);
    }
}
