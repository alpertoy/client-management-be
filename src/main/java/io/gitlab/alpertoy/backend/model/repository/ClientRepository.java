package io.gitlab.alpertoy.backend.model.repository;

import io.gitlab.alpertoy.backend.model.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * User: alpertoy
 * Date: 26.02.2021
 * Time: 10:54
 */
public interface ClientRepository extends JpaRepository<Client, Integer> {
}
