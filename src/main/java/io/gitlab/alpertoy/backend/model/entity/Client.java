package io.gitlab.alpertoy.backend.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * User: alpertoy
 * Date: 26.02.2021
 * Time: 02:31
 */

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 150)
    @NotEmpty(message = "{name.field.required}")
    private String name;

    @Size(max = 11, message = "{number.field.length.required}")
    @Column(nullable = false)
    @NotEmpty(message = "{number.field.required}")
    private String number;

    @Column(name = "register_date", updatable = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate registerDate;

    @PrePersist
    public void prePersist() {
        setRegisterDate(LocalDate.now());
    }
}
