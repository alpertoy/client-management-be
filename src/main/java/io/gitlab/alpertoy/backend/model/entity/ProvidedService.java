package io.gitlab.alpertoy.backend.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * User: alpertoy
 * Date: 26.02.2021
 * Time: 10:35
 */

@Entity
@Data
public class ProvidedService {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 150)
    private String description;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    @Column
    private BigDecimal value;

    @Column
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;
}
