package io.gitlab.alpertoy.backend.rest;

import io.gitlab.alpertoy.backend.exception.UserRegisterException;
import io.gitlab.alpertoy.backend.model.entity.Users;
import io.gitlab.alpertoy.backend.model.repository.UserRepository;
import io.gitlab.alpertoy.backend.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

/**
 * User: alpertoy
 * Date: 6.03.2021
 * Time: 00:41
 */

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController {

    @Autowired
    private UserService service;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void save(@RequestBody @Valid Users users) {
        try {
            service.save(users);
        } catch (UserRegisterException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
