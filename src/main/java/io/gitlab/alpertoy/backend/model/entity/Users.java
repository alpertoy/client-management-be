package io.gitlab.alpertoy.backend.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

/**
 * User: alpertoy
 * Date: 5.03.2021
 * Time: 23:13
 */

@Entity
@Data
@NoArgsConstructor
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true, name = "username")
    @NotEmpty(message = "{client.username.required}")
    private String username;

    @Column(name = "password")
    @NotEmpty(message = "{client.password.required}")
    private String password;


}
