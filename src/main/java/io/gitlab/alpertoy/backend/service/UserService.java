package io.gitlab.alpertoy.backend.service;

import io.gitlab.alpertoy.backend.exception.UserRegisterException;
import io.gitlab.alpertoy.backend.model.entity.Users;
import io.gitlab.alpertoy.backend.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * User: alpertoy
 * Date: 8.03.2021
 * Time: 13:06
 */

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository repository;

    public Users save(Users users) {
        boolean exists = repository.existsByUsername(users.getUsername());
        if(exists) {
            throw new UserRegisterException(users.getUsername());
        }
        return repository.save(users);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users users = repository
                .findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username not found"));
        return User
                .builder()
                .username(users.getUsername())
                .password(users.getPassword())
                .roles("USER")
                .build();
    }
}
