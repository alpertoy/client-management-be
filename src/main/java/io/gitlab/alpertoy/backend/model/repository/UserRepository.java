package io.gitlab.alpertoy.backend.model.repository;

import io.gitlab.alpertoy.backend.model.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * User: alpertoy
 * Date: 6.03.2021
 * Time: 00:18
 */
public interface UserRepository extends JpaRepository<Users, Integer> {

    Optional<Users> findByUsername(String username);

    boolean existsByUsername(String username);
}
