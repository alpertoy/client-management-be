package io.gitlab.alpertoy.backend.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * User: alpertoy
 * Date: 2.03.2021
 * Time: 14:20
 */

@Data
@NoArgsConstructor
public class ProvidedServiceDTO {

    @NotEmpty(message = "{description.field.required}")
    private String description;

    @NotEmpty(message = "{price.field.required}")
    private String price;

    @NotEmpty(message = "{date.field.required}")
    private String date;

    @NotNull(message = "{client.field.required}")
    private Integer idClient;
}
