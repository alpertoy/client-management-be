package io.gitlab.alpertoy.backend.rest.exception;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;

/**
 * User: alpertoy
 * Date: 27.02.2021
 * Time: 03:01
 */
public class ApiErrors {

    @Getter
    private List<String> errors;

    public ApiErrors(List<String> errors) {
        this.errors = errors;
    }

    public ApiErrors(String message) {
        this.errors = Arrays.asList(message);
    }
}
