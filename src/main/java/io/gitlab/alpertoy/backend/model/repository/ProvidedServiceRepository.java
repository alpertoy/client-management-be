package io.gitlab.alpertoy.backend.model.repository;

import io.gitlab.alpertoy.backend.model.entity.ProvidedService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * User: alpertoy
 * Date: 26.02.2021
 * Time: 11:02
 */
public interface ProvidedServiceRepository extends JpaRepository<ProvidedService, Integer> {

    @Query("select s from ProvidedService s join s.client c where upper(c.name) like upper(:name) and MONTH(s.date) =:month")
    List<ProvidedService> findByClientNameAndMonth(@Param("name") String name, @Param("month") Integer month);
}
