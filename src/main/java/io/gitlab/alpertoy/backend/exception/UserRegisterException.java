package io.gitlab.alpertoy.backend.exception;

/**
 * User: alpertoy
 * Date: 9.03.2021
 * Time: 00:11
 */
public class UserRegisterException extends RuntimeException {

    public UserRegisterException(String username) {
        super("User " + username + " already registered");
    }
}
